#!/bin/sh

# Get rid of the locale, needed for svn info and so on.
export LANG=C
export PATCH=90_sync_exporter_for_blender
export QUILT_PATCHES=debian/patches

# Check that the script is run from the toplevel directory
if [ ! -f debian/changelog ]; then
    echo "This script must be run from the toplevel directory"
    exit 1
fi

# Check the availability of needed binaries
for binary in dch dpkg-parsechangelog licensecheck quilt svn; do
    if [ -z $(which $binary) ]; then
	echo "Missing binary in PATH: $binary"
	exit 1
    fi
done

# Needed to get the tag
UPSTREAM_VERSION=$(dpkg-parsechangelog | grep ^Version: | awk '{print $2}' \
    | perl -pe 's/(?:\.dfsg)?-.*?$//')

# Remote and local paths
SVN_URL=https://sunflow.svn.sourceforge.net/svnroot/sunflow
SVN_PATH=exporters/blender/sunflow_export.py

# Revision numbers for the current tagged version and trunk
TAG_REV=$(svn info $SVN_URL/tags/v$UPSTREAM_VERSION/$SVN_PATH \
    | grep '^Last Changed Rev:' | awk '{print $4}')
HEAD_REV=$(svn info $SVN_URL/trunk/$SVN_PATH \
    | grep '^Last Changed Rev:' | awk '{print $4}')

# Possibly informative messages
echo "Upstream version $UPSTREAM_VERSION matches revision $TAG_REV"
echo "Current HEAD for $SVN_PATH matches revision $HEAD_REV"
echo "--------------------------------------------------------------------------------"

# TODO: detect if the patch is no longer needed, and eventually drop
# it (probably on new upstream releases), when $HEAD_REV=$TAG_REV

# This patch is supposed to be the last one
quilt push -a

if [ $(quilt top) != $PATCH ]; then
    # Patch handling
    echo "PATCH CREATION: $PATCH"
    quilt new $PATCH
    quilt add $SVN_PATH

    # Download
    svn cat $SVN_URL/trunk/$SVN_PATH > $SVN_PATH

    # Generate patch, header, changelog entry
    # NOTE: The position of $HEAD_REV is used below to fetch $OLD_REV
    quilt refresh
    echo "Update from $TAG_REV to $HEAD_REV" \
	| quilt header -a $PATCH
    ACTION="Added patch"

    # Friendly reminder
    echo "--------------------------------------------------------------------------------"
    echo "DON'T FORGET TO GIT-ADD IT"
else
    # Fetch the old revision from the patch header
    # NOTE: $5 matches the position of the new revision in the header
    OLD_REV=$(quilt header $PATCH | awk '{print $5}')

    # Detect being already up-to-date
    if [ $OLD_REV = $HEAD_REV ]; then
	echo "Nothing to do, already up-to-date thanks to the current patch"
	exit
    fi

    # Download
    svn cat $SVN_URL/trunk/$SVN_PATH > $SVN_PATH

    # (re)generate patch, header, changelog entry
    quilt refresh
    echo "Update from $TAG_REV to $HEAD_REV" \
	| quilt header -r $PATCH
    ACTION="Refreshed patch"
fi

# Extract the version, expecting the format to stay stable
VERSION=$(grep ^Version exporters/blender/sunflow_export.py \
    | head -n1 | awk '{print $3}')

# Dirty hack since dch doesn't support passing newlines
dch -t -a "$ACTION, updating the Sunflow Exporter for Blender (sync from r$TAG_REV to r$HEAD_REV), new version is $VERSION:"
dch -t -a " - debian/patches/$PATCH."
sed -i -e 's/^  \*  - /     - /' debian/changelog

# Also check that the license didn't change
LICENSE=$(licensecheck $SVN_PATH)
if [ $(echo $LICENSE | grep -c 'GPL (v2 or later)') != 1 ]; then
    echo "WARNING: the license is no longer 'GPL (v2 or later)'"
    echo "licensecheck reports: $LICENSE"
fi
